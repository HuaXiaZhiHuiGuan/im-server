package cn.slb.im.user.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.slb.im.common.utils.PageUtils;
import cn.slb.im.common.utils.Query;

import cn.slb.im.user.dao.DeptUserDao;
import cn.slb.im.user.entity.DeptUserEntity;
import cn.slb.im.user.service.DeptUserService;


@Service("deptUserService")
public class DeptUserServiceImpl extends ServiceImpl<DeptUserDao, DeptUserEntity> implements DeptUserService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<DeptUserEntity> page = this.page(
                new Query<DeptUserEntity>().getPage(params),
                new QueryWrapper<DeptUserEntity>()
        );

        return new PageUtils(page);
    }

}