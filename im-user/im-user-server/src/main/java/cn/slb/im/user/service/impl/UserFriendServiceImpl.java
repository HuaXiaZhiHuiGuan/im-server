package cn.slb.im.user.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.slb.im.common.utils.PageUtils;
import cn.slb.im.common.utils.Query;

import cn.slb.im.user.dao.UserFriendDao;
import cn.slb.im.user.entity.UserFriendEntity;
import cn.slb.im.user.service.UserFriendService;


@Service("userFriendService")
public class UserFriendServiceImpl extends ServiceImpl<UserFriendDao, UserFriendEntity> implements UserFriendService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<UserFriendEntity> page = this.page(
                new Query<UserFriendEntity>().getPage(params),
                new QueryWrapper<UserFriendEntity>()
        );

        return new PageUtils(page);
    }

}