package cn.slb.im.user.utils;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @Author: NickLi
 * @Description: 加密、解密工具类
 * @Date: 2021/12/4 16:19
 */
public class BcryptUtil {


    /**
     * 加密
     * @param ps
     * @return
     */
    public static String encode(String ps){
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder.encode(ps);
    }

    /**
     * 比对
     * @param source
     * @param target
     * @return
     */
    public static boolean match(String source,String target){
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder.matches(source, target);
    }


}
