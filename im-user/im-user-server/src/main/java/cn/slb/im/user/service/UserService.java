package cn.slb.im.user.service;

import cn.slb.im.user.dto.LoginUserDTO;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.slb.im.common.utils.PageUtils;
import cn.slb.im.user.entity.UserEntity;

import java.util.List;
import java.util.Map;

/**
 * 用户表
 *
 * @author nick
 * @email ld@qq.com
 * @date 2021-12-03 00:01:54
 */
public interface UserService extends IService<UserEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 保存用户
     * @param user
     */
    void saveUser(UserEntity user);

    /**
     * 删除用户，逻辑删除
     * @param asList
     */
    void deleteByIds(List<Long> asList);

    /**
     * 重置密码
     * @param id
     */
    void resetPs(Long id);

    /**
     *
     * @param loginUserDTO
     * @return
     */
    UserEntity login(LoginUserDTO loginUserDTO);
}

