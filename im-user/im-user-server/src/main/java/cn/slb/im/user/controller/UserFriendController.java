package cn.slb.im.user.controller;

import cn.slb.im.common.utils.PageUtils;
import cn.slb.im.common.utils.R;
import cn.slb.im.user.entity.UserFriendEntity;
import cn.slb.im.user.service.UserFriendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.Map;


/**
 * 用户-朋友关联表
 *
 * @author nick
 * @email ld@qq.com
 * @date 2021-12-03 00:01:54
 */
@RestController
@RequestMapping("user/userfriend")
public class UserFriendController {
    @Autowired
    private UserFriendService userFriendService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("user:userfriend:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = userFriendService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{userId}")
    //@RequiresPermissions("user:userfriend:info")
    public R info(@PathVariable("userId") Long userId) {
        UserFriendEntity userFriend = userFriendService.getById(userId);

        return R.ok().put("userFriend", userFriend);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("user:userfriend:save")
    public R save(@RequestBody UserFriendEntity userFriend) {
        userFriendService.save(userFriend);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("user:userfriend:update")
    public R update(@RequestBody UserFriendEntity userFriend) {
        userFriendService.updateById(userFriend);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("user:userfriend:delete")
    public R delete(@RequestBody Long[] userIds) {
        userFriendService.removeByIds(Arrays.asList(userIds));

        return R.ok();
    }

}
