package cn.slb.im.user.service.impl;

import cn.slb.im.common.utils.BussinessException;
import cn.slb.im.common.utils.PageUtils;
import cn.slb.im.common.utils.Query;
import cn.slb.im.common.utils.SystemException;
import cn.slb.im.user.config.UserConfig;
import cn.slb.im.user.dao.UserDao;
import cn.slb.im.user.dto.LoginUserDTO;
import cn.slb.im.user.entity.UserEntity;
import cn.slb.im.user.enums.LoginEnum;
import cn.slb.im.user.service.UserService;
import cn.slb.im.user.utils.BcryptUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@AllArgsConstructor
@Service("userService")
public class UserServiceImpl extends ServiceImpl<UserDao, UserEntity> implements UserService {

    private final UserConfig userConfig;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        LambdaQueryWrapper<UserEntity> queryWrapper = new LambdaQueryWrapper<UserEntity>();
        queryWrapper.eq(UserEntity::getIsDelete, 0);
        Object key = params.get("key");
        if (!ObjectUtils.isEmpty(key)) {
            queryWrapper.eq(UserEntity::getId, key)
                    .or().like(UserEntity::getName, key)
                    .or().like(UserEntity::getNickName, key)
                    .or().like(UserEntity::getMobile, key)
                    .or().like(UserEntity::getEmail, key)
                    .or().like(UserEntity::getDeptName, key);
        }
        IPage<UserEntity> page = this.page(
                new Query<UserEntity>().getPage(params),
                queryWrapper);
        return new PageUtils(page);
    }

    @Override
    public void saveUser(UserEntity user) {
        // 初始登录状态为：未在线
        user.setLoginStatus(LoginEnum.NO_ONLINE.getCode());
        // 初始密码
        user.setPassword(BcryptUtil.encode(userConfig.getInitPassword()));
        this.save(user);
    }

    @Override
    public void deleteByIds(List<Long> ids) {
        List<UserEntity> users = new ArrayList<>();
        ids.forEach(id -> {
            UserEntity user = new UserEntity();
            user.setId(id);
            user.setIsDelete(1);
            users.add(user);
        });
        this.updateBatchById(users);
    }

    @Override
    public void resetPs(Long id) {
        UserEntity user = new UserEntity();
        user.setId(id);
        user.setPassword(BcryptUtil.encode(userConfig.getInitPassword()));
        this.updateById(user);
    }

    @Override
    public UserEntity login(LoginUserDTO loginUser) {
        UserEntity user = this.getOne(new LambdaQueryWrapper<UserEntity>()
                .eq(UserEntity::getMobile, loginUser.getLoginAccount())
                .or().eq(UserEntity::getLoginAccount, loginUser.getLoginAccount()));
        if(ObjectUtils.isEmpty(user)){
            throw new BussinessException("账号错误，请重新输入。");
        }
        if(!BcryptUtil.match(loginUser.getPassword(),user.getPassword())){
            throw new BussinessException("密码错误，请重新输入。");
        }
        return user;
    }
}
