package cn.slb.im.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.slb.im.common.utils.PageUtils;
import cn.slb.im.user.entity.UserFriendEntity;

import java.util.Map;

/**
 * 用户-朋友关联表
 *
 * @author nick
 * @email ld@qq.com
 * @date 2021-12-03 00:01:54
 */
public interface UserFriendService extends IService<UserFriendEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

