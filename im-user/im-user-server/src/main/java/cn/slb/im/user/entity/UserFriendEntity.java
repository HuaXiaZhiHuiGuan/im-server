package cn.slb.im.user.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * 用户-朋友关联表
 *
 * @author nick
 * @email ld@qq.com
 * @date 2021-12-03 00:01:54
 */
@Data
@TableName("im_user_friend")
public class UserFriendEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 用户主键
	 */
	@TableId
	private Long userId;
	/**
	 * 朋友主键
	 */
	private Long friendId;
	/**
	 * 朋友昵称
	 */
	private String userNickName;
	/**
	 * 朋友昵称
	 */
	private String friendNickName;
	/**
	 * 是否删除，0 否 1是
	 */
	private Integer isDelete;
	/**
	 * 创建时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;
	/**
	 * 更新时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date updateTime;

}
