package cn.slb.im.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.slb.im.common.utils.PageUtils;
import cn.slb.im.user.entity.DeptEntity;

import java.util.List;
import java.util.Map;

/**
 * 部门表
 *
 * @author nick
 * @email ld@qq.com
 * @date 2021-12-03 00:01:54
 */
public interface DeptService extends IService<DeptEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 查出所有部门，包括所有子部门，封装成树型结构组装起来
     * @param params
     * @return
     */
    List<DeptEntity> listTree(Map<String, Object> params);

    /**
     * 逻辑删除
     * @param ids
     */
    void deleteByIds(Long[] ids);
}

