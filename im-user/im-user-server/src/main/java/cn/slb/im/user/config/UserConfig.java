package cn.slb.im.user.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Author: NickLi
 * @Description: 用户服务配置
 * @Date: 2021/12/4 16:09
 */

@Data
@Component
@ConfigurationProperties("user")
public class UserConfig {

    private String initPassword;

}
