package cn.slb.im.user;

import cn.slb.im.common.config.EnableCommonConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author nick
 */
@EnableCommonConfig
@EnableDiscoveryClient
@SpringBootApplication
public class ImUserServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ImUserServerApplication.class, args);
    }

}
