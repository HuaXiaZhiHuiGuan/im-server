package cn.slb.im.user.controller;

import cn.slb.im.common.utils.PageUtils;
import cn.slb.im.common.utils.R;
import cn.slb.im.user.entity.DeptUserEntity;
import cn.slb.im.user.service.DeptUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.Map;


/**
 * 部门-用户关联表
 *
 * @author nick
 * @email ld@qq.com
 * @date 2021-12-03 00:01:54
 */
@RestController
@RequestMapping("user/deptuser")
public class DeptUserController {
    @Autowired
    private DeptUserService deptUserService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("user:deptuser:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = deptUserService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{deptId}")
    //@RequiresPermissions("user:deptuser:info")
    public R info(@PathVariable("deptId") Long deptId) {
        DeptUserEntity deptUser = deptUserService.getById(deptId);

        return R.ok().put("deptUser", deptUser);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("user:deptuser:save")
    public R save(@RequestBody DeptUserEntity deptUser) {
        deptUserService.save(deptUser);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("user:deptuser:update")
    public R update(@RequestBody DeptUserEntity deptUser) {
        deptUserService.updateById(deptUser);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("user:deptuser:delete")
    public R delete(@RequestBody Long[] deptIds) {
        deptUserService.removeByIds(Arrays.asList(deptIds));

        return R.ok();
    }

}
