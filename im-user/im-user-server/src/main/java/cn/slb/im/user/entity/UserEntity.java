package cn.slb.im.user.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * 用户表
 *
 * @author nick
 * @email ld@qq.com
 * @date 2021-12-03 00:01:54
 */
@Data
@TableName("im_user")
public class UserEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	@JsonFormat(shape= JsonFormat.Shape.STRING)
	private Long id;
	/**
	 * 用户姓名
	 */
	private String name;

	@JsonFormat(shape= JsonFormat.Shape.STRING)
	private Long deptId;

	private String deptName;

	/**
	 * 手机号码
	 */
	private String mobile;
	/**
	 * 邮箱地址
	 */
	private String email;
	/**
	 * 用户出生日期，yyyy-MM-dd
	 */
	private String birthday;
	/**
	 * 用户头像
	 */
	private String userImgUrl;
	/**
	 * 登录昵称
	 */
	private String nickName;

	/**
	 * 登录账号
	 */
	private String loginAccount;

	/**
	 * 登录密码
	 */
	private String password;
	/**
	 * 登录状态
	 */
	private Integer loginStatus;
	/**
	 * 是否删除，0 否 1是
	 */
	private Integer isDelete;
	/**
	 * 创建时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;
	/**
	 * 更新时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date updateTime;

}
