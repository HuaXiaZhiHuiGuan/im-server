package cn.slb.im.user.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * 部门-用户关联表
 *
 * @author nick
 * @email ld@qq.com
 * @date 2021-12-03 00:01:54
 */
@Data
@TableName("im_dept_user")
public class DeptUserEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 部门主键
	 */
	@TableId
	private Long deptId;
	/**
	 * 用户主键
	 */
	private Long userId;
	/**
	 * 部门编码
	 */
	private String deptCode;
	/**
	 * 部门名称
	 */
	private String deptName;
	/**
	 * 用户姓名
	 */
	private String name;
	/**
	 * 是否删除，0 否 1是
	 */
	private String isDelete;
	/**
	 * 创建时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;
	/**
	 * 更新时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date updateTime;

}
