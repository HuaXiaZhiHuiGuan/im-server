package cn.slb.im.user.enums;

import lombok.Data;
import lombok.Getter;

@Getter
public enum LoginEnum {

    /**
     * 1,在线
     * 0,未在线
     */
    ONLINE(1,"在线"),
    NO_ONLINE(0,"未在线"),

    ;

    private int code;
    private String desc;

    LoginEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
