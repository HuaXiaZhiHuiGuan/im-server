package cn.slb.im.user.dao;

import cn.slb.im.user.entity.DeptUserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 部门-用户关联表
 * 
 * @author nick
 * @email ld@qq.com
 * @date 2021-12-03 00:01:54
 */
@Mapper
public interface DeptUserDao extends BaseMapper<DeptUserEntity> {
	
}
