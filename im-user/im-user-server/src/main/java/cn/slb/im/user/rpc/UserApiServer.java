package cn.slb.im.user.rpc;

import cn.slb.im.common.utils.R;
import cn.slb.im.user.api.UserApi;
import cn.slb.im.user.dto.LoginUserDTO;
import cn.slb.im.user.service.UserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: NickLi
 * @Description: 用户服务 api 接口实现
 * @Date: 2021/12/6 17:27
 */
@Slf4j
@RestController
@AllArgsConstructor
public class UserApiServer implements UserApi {

    private final UserService userService;

    /**
     * 用户登录操作
     *
     * @param loginUserDTO
     * @return
     */
    @Override
    public R login(@RequestBody LoginUserDTO loginUserDTO) {
        return R.ok(userService.login(loginUserDTO));
    }
}
