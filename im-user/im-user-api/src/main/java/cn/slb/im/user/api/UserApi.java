package cn.slb.im.user.api;

import cn.slb.im.common.utils.R;
import cn.slb.im.user.dto.LoginUserDTO;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Author: NickLi
 * @Description: 用户服务 api
 * @Date: 2021/12/6 17:24
 */

@RequestMapping("/rpc/user")
public interface UserApi {

    /**
     * 用户登录
     * @param loginUserDTO
     * @return
     */
    @PostMapping("/login")
    R login(@RequestBody LoginUserDTO loginUserDTO);

}
