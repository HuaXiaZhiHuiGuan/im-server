package cn.slb.im.user.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author: NickLi
 * @Description: 登录用户实体类
 * @Date: 2021/12/6 17:30
 */
@Data
public class LoginUserDTO implements Serializable {

    /**
     * 登录账号
     */
    private String loginAccount;

    /**
     * 登录密码
     */
    private String password;

}
