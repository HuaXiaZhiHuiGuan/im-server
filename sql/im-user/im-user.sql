-- 部门表
CREATE TABLE `im_dept` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `dept_code` varchar(32) NOT NULL COMMENT '部门编码',
  `dept_name` varchar(255) DEFAULT NULL COMMENT '部门名称',
  `parent_id` bigint(20) NOT NULL COMMENT '父级部门',
  `is_delete` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否删除，0 否 1是',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT '部门表';

-- 用户表
CREATE TABLE `im_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) DEFAULT NULL COMMENT '用户姓名',
  `mobile` varchar(255) DEFAULT NULL COMMENT '手机号码',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱地址',
  `birthday` varchar(32) DEFAULT NULL COMMENT '用户出生日期，yyyy-MM-dd',
  `user_img_url` varchar(255) DEFAULT NULL COMMENT '用户头像',
  `nick_name` varchar(255) DEFAULT NULL COMMENT '登录昵称',
  `password` varchar(255) DEFAULT NULL COMMENT '登录密码',
  `login_status` tinyint(4) DEFAULT NULL COMMENT '登录状态',
  `is_delete` tinyint(4) DEFAULT '0' COMMENT '是否删除，0 否 1是',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='用户表';

-- 部门-用户关联表
CREATE TABLE `im_dept_user` (
  `dept_id` bigint(20) NOT NULL COMMENT '部门主键',
  `user_id` bigint(20) NOT NULL COMMENT '用户主键',
  `dept_code` varchar(32) NOT NULL COMMENT '部门编码',
  `dept_name` varchar(255) DEFAULT NULL COMMENT '部门名称',
  `name` varchar(255) DEFAULT NULL COMMENT '用户姓名',
  `is_delete` varchar(255) NOT NULL DEFAULT '0' COMMENT '是否删除，0 否 1是',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`dept_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='部门-用户关联表';

-- 用户-朋友关联表
CREATE TABLE `im_user_friend` (
  `user_id` bigint(20) NOT NULL COMMENT '用户主键',
  `friend_id` bigint(20) NOT NULL COMMENT '朋友主键',
  `user_nick_name` varchar(255) DEFAULT NULL COMMENT '朋友昵称',
  `friend_nick_name` varchar(255) DEFAULT NULL COMMENT '朋友昵称',
  `is_delete` tinyint(4) DEFAULT '0' COMMENT '是否删除，0 否 1是',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`user_id`,`friend_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT '用户-朋友关联表';
