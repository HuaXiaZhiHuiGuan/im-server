/*
 Navicat Premium Data Transfer

 Source Server         : 本地mysql
 Source Server Type    : MySQL
 Source Server Version : 50717
 Source Host           : localhost:3306
 Source Schema         : im_management

 Target Server Type    : MySQL
 Target Server Version : 50717
 File Encoding         : 65001

 Date: 07/12/2021 23:11:39
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_captcha
-- ----------------------------
DROP TABLE IF EXISTS `sys_captcha`;
CREATE TABLE `sys_captcha`  (
  `uuid` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'uuid',
  `code` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '验证码',
  `expire_time` datetime(0) NULL DEFAULT NULL COMMENT '过期时间',
  PRIMARY KEY (`uuid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统验证码' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_captcha
-- ----------------------------
INSERT INTO `sys_captcha` VALUES ('07ae2239-1fb5-4663-8ccf-7d6023435c35', '2n7bn', '2021-12-05 01:21:21');
INSERT INTO `sys_captcha` VALUES ('182c2651-827d-43b0-8cac-62db9c2a5284', 'p6gmg', '2021-12-04 23:39:35');
INSERT INTO `sys_captcha` VALUES ('356094f7-8fa2-406a-8e7d-faae1a603ecf', 'eg7fa', '2021-12-02 22:09:17');
INSERT INTO `sys_captcha` VALUES ('727cb1d6-f219-4bca-82a3-35f16d9e7b5c', 'pne6n', '2021-12-03 00:47:32');
INSERT INTO `sys_captcha` VALUES ('7dda82a1-a1f2-4435-8455-9b543bac0aa7', '7ecc7', '2021-12-02 22:09:19');
INSERT INTO `sys_captcha` VALUES ('95acf1e6-bcf0-49ca-8ca8-b0a6ae083a7b', 'mxa34', '2021-12-03 20:44:37');
INSERT INTO `sys_captcha` VALUES ('9743ddcb-651b-4651-81c1-278dd5aa3824', 'fydmc', '2021-12-03 20:47:25');
INSERT INTO `sys_captcha` VALUES ('979393d6-31b4-4705-8ce9-f2e76c3a5296', 'yegya', '2021-12-03 20:47:27');
INSERT INTO `sys_captcha` VALUES ('ac6a7b2b-7806-44a1-828f-62223078fb1d', 'm5be2', '2021-12-03 20:47:27');
INSERT INTO `sys_captcha` VALUES ('af0822a8-a81c-4e28-8b6c-382cc432c600', '8gam7', '2021-12-07 22:13:35');
INSERT INTO `sys_captcha` VALUES ('cf055332-cccb-45b1-82ea-dac3173a6d9e', 'y8a7b', '2021-12-02 22:46:10');
INSERT INTO `sys_captcha` VALUES ('d8703c68-285b-4457-8e3d-91553d8ba1b3', 'mp5aw', '2021-12-03 20:47:05');
INSERT INTO `sys_captcha` VALUES ('d9f6dfd1-6568-473b-8cfb-63bd98d54b7a', 'mb67m', '2021-12-04 23:40:43');
INSERT INTO `sys_captcha` VALUES ('e9847b5e-81c7-4d32-88f6-6b1cfcd72726', 'nnm7f', '2021-12-02 22:45:50');
INSERT INTO `sys_captcha` VALUES ('ea580149-fac7-480f-8230-5ff256a43166', 'ga4cb', '2021-12-03 20:47:26');

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `param_key` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'key',
  `param_value` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'value',
  `status` tinyint(4) NULL DEFAULT 1 COMMENT '状态   0：隐藏   1：显示',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `param_key`(`param_key`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统配置信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, 'CLOUD_STORAGE_CONFIG_KEY', '{\"aliyunAccessKeyId\":\"\",\"aliyunAccessKeySecret\":\"\",\"aliyunBucketName\":\"\",\"aliyunDomain\":\"\",\"aliyunEndPoint\":\"\",\"aliyunPrefix\":\"\",\"qcloudBucketName\":\"\",\"qcloudDomain\":\"\",\"qcloudPrefix\":\"\",\"qcloudSecretId\":\"\",\"qcloudSecretKey\":\"\",\"qiniuAccessKey\":\"NrgMfABZxWLo5B-YYSjoE8-AZ1EISdi1Z3ubLOeZ\",\"qiniuBucketName\":\"ios-app\",\"qiniuDomain\":\"http://7xqbwh.dl1.z0.glb.clouddn.com\",\"qiniuPrefix\":\"upload\",\"qiniuSecretKey\":\"uIwJHevMRWU0VLxFvgy0tAcOdGqasdtVlJkdy6vV\",\"type\":1}', 0, '云存储配置信息');

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `operation` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户操作',
  `method` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求参数',
  `time` bigint(20) NOT NULL COMMENT '执行时长(毫秒)',
  `ip` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'IP地址',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_log
-- ----------------------------
INSERT INTO `sys_log` VALUES (1, 'admin', '保存菜单', 'io.renren.modules.sys.controller.SysMenuController.save()', '[{\"menuId\":51,\"parentId\":0,\"name\":\"用户管理\",\"url\":\"\",\"perms\":\"\",\"type\":0,\"icon\":\"admin\",\"orderNum\":0,\"list\":[]}]', 17, '0:0:0:0:0:0:0:1', '2021-12-03 00:47:38');
INSERT INTO `sys_log` VALUES (2, 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":31,\"parentId\":51,\"name\":\"部门表\",\"url\":\"user/dept\",\"type\":1,\"icon\":\"shouye\",\"orderNum\":6,\"list\":[]}]', 8, '0:0:0:0:0:0:0:1', '2021-12-03 00:48:30');
INSERT INTO `sys_log` VALUES (3, 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":36,\"parentId\":51,\"name\":\"部门-用户关联表\",\"url\":\"user/deptuser\",\"type\":1,\"icon\":\"menu\",\"orderNum\":6,\"list\":[]}]', 5, '0:0:0:0:0:0:0:1', '2021-12-03 00:48:59');
INSERT INTO `sys_log` VALUES (4, 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":41,\"parentId\":51,\"name\":\"用户表\",\"url\":\"user/user\",\"type\":1,\"icon\":\"config\",\"orderNum\":6,\"list\":[]}]', 4, '0:0:0:0:0:0:0:1', '2021-12-03 00:49:13');
INSERT INTO `sys_log` VALUES (5, 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":46,\"parentId\":51,\"name\":\"用户-朋友关联表\",\"url\":\"user/userfriend\",\"type\":1,\"icon\":\"config\",\"orderNum\":6,\"list\":[]}]', 8, '0:0:0:0:0:0:0:1', '2021-12-03 00:49:23');
INSERT INTO `sys_log` VALUES (6, 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":41,\"parentId\":51,\"name\":\"用户表\",\"url\":\"user/user\",\"type\":1,\"icon\":\"geren\",\"orderNum\":6,\"list\":[]}]', 11, '0:0:0:0:0:0:0:1', '2021-12-03 00:49:39');
INSERT INTO `sys_log` VALUES (7, 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":46,\"parentId\":51,\"name\":\"用户-朋友关联表\",\"url\":\"user/userfriend\",\"type\":1,\"icon\":\"geren\",\"orderNum\":6,\"list\":[]}]', 26, '0:0:0:0:0:0:0:1', '2021-12-03 00:49:49');
INSERT INTO `sys_log` VALUES (8, 'admin', '保存菜单', 'io.renren.modules.sys.controller.SysMenuController.save()', '[{\"menuId\":52,\"parentId\":0,\"name\":\"聊天管理\",\"url\":\"\",\"perms\":\"\",\"type\":0,\"icon\":\"tixing\",\"orderNum\":0,\"list\":[]}]', 5, '0:0:0:0:0:0:0:1', '2021-12-03 00:55:11');
INSERT INTO `sys_log` VALUES (9, 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":31,\"parentId\":51,\"name\":\"部门\",\"url\":\"user/dept\",\"type\":1,\"icon\":\"shouye\",\"orderNum\":6,\"list\":[]}]', 5, '0:0:0:0:0:0:0:1', '2021-12-03 00:58:10');
INSERT INTO `sys_log` VALUES (10, 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":41,\"parentId\":51,\"name\":\"用户\",\"url\":\"user/user\",\"type\":1,\"icon\":\"geren\",\"orderNum\":6,\"list\":[]}]', 5, '0:0:0:0:0:0:0:1', '2021-12-03 00:58:37');
INSERT INTO `sys_log` VALUES (11, 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":46,\"parentId\":51,\"name\":\"用户关系\",\"url\":\"user/userfriend\",\"type\":1,\"icon\":\"geren\",\"orderNum\":6,\"list\":[]}]', 3, '0:0:0:0:0:0:0:1', '2021-12-03 00:59:18');
INSERT INTO `sys_log` VALUES (12, 'admin', '删除菜单', 'io.renren.modules.sys.controller.SysMenuController.delete()', '[36]', 8, '0:0:0:0:0:0:0:1', '2021-12-05 01:15:46');
INSERT INTO `sys_log` VALUES (13, 'admin', '删除菜单', 'io.renren.modules.sys.controller.SysMenuController.delete()', '[37]', 26, '0:0:0:0:0:0:0:1', '2021-12-05 01:15:53');
INSERT INTO `sys_log` VALUES (14, 'admin', '删除菜单', 'io.renren.modules.sys.controller.SysMenuController.delete()', '[38]', 8, '0:0:0:0:0:0:0:1', '2021-12-05 01:15:57');
INSERT INTO `sys_log` VALUES (15, 'admin', '删除菜单', 'io.renren.modules.sys.controller.SysMenuController.delete()', '[39]', 7, '0:0:0:0:0:0:0:1', '2021-12-05 01:16:01');
INSERT INTO `sys_log` VALUES (16, 'admin', '删除菜单', 'io.renren.modules.sys.controller.SysMenuController.delete()', '[40]', 10, '0:0:0:0:0:0:0:1', '2021-12-05 01:16:05');
INSERT INTO `sys_log` VALUES (17, 'admin', '删除菜单', 'io.renren.modules.sys.controller.SysMenuController.delete()', '[36]', 15, '0:0:0:0:0:0:0:1', '2021-12-05 01:16:09');
INSERT INTO `sys_log` VALUES (18, 'admin', '删除菜单', 'io.renren.modules.sys.controller.SysMenuController.delete()', '[14]', 0, '0:0:0:0:0:0:0:1', '2021-12-07 22:50:05');
INSERT INTO `sys_log` VALUES (19, 'admin', '删除菜单', 'io.renren.modules.sys.controller.SysMenuController.delete()', '[13]', 0, '0:0:0:0:0:0:0:1', '2021-12-07 22:50:07');
INSERT INTO `sys_log` VALUES (20, 'admin', '删除菜单', 'io.renren.modules.sys.controller.SysMenuController.delete()', '[12]', 1, '0:0:0:0:0:0:0:1', '2021-12-07 22:50:10');
INSERT INTO `sys_log` VALUES (21, 'admin', '删除菜单', 'io.renren.modules.sys.controller.SysMenuController.delete()', '[10]', 0, '0:0:0:0:0:0:0:1', '2021-12-07 22:50:13');
INSERT INTO `sys_log` VALUES (22, 'admin', '删除菜单', 'io.renren.modules.sys.controller.SysMenuController.delete()', '[7]', 0, '0:0:0:0:0:0:0:1', '2021-12-07 22:51:34');
INSERT INTO `sys_log` VALUES (23, 'admin', '删除菜单', 'io.renren.modules.sys.controller.SysMenuController.delete()', '[30]', 0, '0:0:0:0:0:0:0:1', '2021-12-07 22:52:39');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) NULL DEFAULT NULL COMMENT '父菜单ID，一级菜单为0',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单URL',
  `perms` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '授权(多个用逗号分隔，如：user:list,user:create)',
  `type` int(11) NULL DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
  `icon` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单图标',
  `order_num` int(11) NULL DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 53 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '菜单管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, 0, '系统管理', NULL, NULL, 0, 'system', 0);
INSERT INTO `sys_menu` VALUES (2, 1, '管理员列表', 'sys/user', NULL, 1, 'admin', 1);
INSERT INTO `sys_menu` VALUES (3, 1, '角色管理', 'sys/role', NULL, 1, 'role', 2);
INSERT INTO `sys_menu` VALUES (4, 1, '菜单管理', 'sys/menu', NULL, 1, 'menu', 3);
INSERT INTO `sys_menu` VALUES (5, 1, 'SQL监控', 'http://localhost:8000/management/druid/sql.html', NULL, 1, 'sql', 4);
INSERT INTO `sys_menu` VALUES (15, 2, '查看', NULL, 'sys:user:list,sys:user:info', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (16, 2, '新增', NULL, 'sys:user:save,sys:role:select', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (17, 2, '修改', NULL, 'sys:user:update,sys:role:select', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (18, 2, '删除', NULL, 'sys:user:delete', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (19, 3, '查看', NULL, 'sys:role:list,sys:role:info', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (20, 3, '新增', NULL, 'sys:role:save,sys:menu:list', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (21, 3, '修改', NULL, 'sys:role:update,sys:menu:list', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (22, 3, '删除', NULL, 'sys:role:delete', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (23, 4, '查看', NULL, 'sys:menu:list,sys:menu:info', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (24, 4, '新增', NULL, 'sys:menu:save,sys:menu:select', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (25, 4, '修改', NULL, 'sys:menu:update,sys:menu:select', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (26, 4, '删除', NULL, 'sys:menu:delete', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (27, 1, '参数管理', 'sys/config', 'sys:config:list,sys:config:info,sys:config:save,sys:config:update,sys:config:delete', 1, 'config', 6);
INSERT INTO `sys_menu` VALUES (29, 1, '系统日志', 'sys/log', 'sys:log:list', 1, 'log', 7);
INSERT INTO `sys_menu` VALUES (31, 51, '部门', 'user/dept', NULL, 1, 'shouye', 6);
INSERT INTO `sys_menu` VALUES (32, 31, '查看', NULL, 'user:dept:list,user:dept:info', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (33, 31, '新增', NULL, 'user:dept:save', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (34, 31, '修改', NULL, 'user:dept:update', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (35, 31, '删除', NULL, 'user:dept:delete', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (41, 51, '用户', 'user/user', NULL, 1, 'geren', 6);
INSERT INTO `sys_menu` VALUES (42, 41, '查看', NULL, 'user:user:list,user:user:info', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (43, 41, '新增', NULL, 'user:user:save', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (44, 41, '修改', NULL, 'user:user:update', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (45, 41, '删除', NULL, 'user:user:delete', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (46, 51, '用户关系', 'user/userfriend', NULL, 1, 'geren', 6);
INSERT INTO `sys_menu` VALUES (47, 46, '查看', NULL, 'user:userfriend:list,user:userfriend:info', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (48, 46, '新增', NULL, 'user:userfriend:save', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (49, 46, '修改', NULL, 'user:userfriend:update', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (50, 46, '删除', NULL, 'user:userfriend:delete', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (51, 0, '用户管理', '', '', 0, 'admin', 0);
INSERT INTO `sys_menu` VALUES (52, 0, '聊天管理', '', '', 0, 'tixing', 0);

-- ----------------------------
-- Table structure for sys_oss
-- ----------------------------
DROP TABLE IF EXISTS `sys_oss`;
CREATE TABLE `sys_oss`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'URL地址',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '文件上传' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色名称',
  `remark` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_user_id` bigint(20) NULL DEFAULT NULL COMMENT '创建者ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) NULL DEFAULT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NULL DEFAULT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色与菜单对应关系' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `salt` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '盐',
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '状态  0：禁用   1：正常',
  `create_user_id` bigint(20) NULL DEFAULT NULL COMMENT '创建者ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统用户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'admin', '9ec9750e709431dad22365cabc5c625482e574c74adaebba7dd02f1129e4ce1d', 'YzcmCZNvbXocrsz9dm8e', 'root@renren.io', '13612345678', 1, 1, '2016-11-11 11:11:11');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户ID',
  `role_id` bigint(20) NULL DEFAULT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户与角色对应关系' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_user_token
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_token`;
CREATE TABLE `sys_user_token`  (
  `user_id` bigint(20) NOT NULL,
  `token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'token',
  `expire_time` datetime(0) NULL DEFAULT NULL COMMENT '过期时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `token`(`token`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统用户Token' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_token
-- ----------------------------
INSERT INTO `sys_user_token` VALUES (1, 'b3a2a59e4f1cb923bc058ec2984c22d3', '2021-12-08 10:49:44', '2021-12-07 22:49:44');

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名',
  `mobile` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '手机号',
  `password` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES (1, 'mark', '13612345678', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', '2017-03-23 22:37:41');

SET FOREIGN_KEY_CHECKS = 1;
