package cn.slb.im.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 通用响应枚举类
 *
 * @author nick
 */

@Getter
@AllArgsConstructor
public enum ExceptionCodeEnum {
    /**
     * code 编码
     * msg 信息
     */
    ERROR_500(500, "网络超时，请稍后再试。"),
    ERROR_100(100, "系统异常，异常信息："),
    ERROR_101(101, "业务异常，异常信息："),
    ERROR_102(102, "Feign调用异常，异常信息："),

    ;

    private int code;
    private String msg;
}
