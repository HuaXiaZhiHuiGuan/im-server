package cn.slb.im.common.config;

import cn.slb.im.common.enums.ExceptionCodeEnum;
import cn.slb.im.common.utils.BussinessException;
import cn.slb.im.common.utils.R;
import cn.slb.im.common.utils.SystemException;
import feign.FeignException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @Author: NickLi
 * @Description: 全局异常处理
 * @Date: 2021/12/7 9:38
 */

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {


    /**
     * 系统异常
     *
     * @param sex 自定义系统异常
     * @return 通用返回
     */
    @ExceptionHandler(value = SystemException.class)
    public R exceptionHandler(SystemException sex) {
        log.error(ExceptionCodeEnum.ERROR_100.getMsg() + sex.getMessage(), sex);
        return R.error(ExceptionCodeEnum.ERROR_100.getCode(), ExceptionCodeEnum.ERROR_100.getMsg() + sex.getMessage());
    }

    /**
     * 业务异常
     *
     * @param bex 自定义业务异常
     * @return 通用返回
     */
    @ExceptionHandler(value = BussinessException.class)
    public R exceptionHandler(BussinessException bex) {
        log.warn(ExceptionCodeEnum.ERROR_101.getMsg() + bex.getMsg());
        return R.error(ExceptionCodeEnum.ERROR_101.getCode(), bex.getMessage());
    }


    /**
     * FEIGN 调用异常
     *
     * @param fe FEIGN 调用异常
     * @return 通用返回
     */
    @ExceptionHandler(FeignException.class)
    public R handlerError(FeignException fe) {
        log.error(ExceptionCodeEnum.ERROR_102.getMsg(), fe.getMessage(), fe);
        return R.error(ExceptionCodeEnum.ERROR_102.getCode(), fe.getMessage());
    }

    /**
     * Exception 异常
     *
     * @param ex Exception 异常
     * @return 通用返回
     */
    @ExceptionHandler(value = Exception.class)
    public R exceptionHandler(Exception ex) {
        log.error("发生 Exception 异常，异常信息：{},异常堆栈：", ex.getMessage(), ex);
        return R.error(ExceptionCodeEnum.ERROR_500.getCode(), ExceptionCodeEnum.ERROR_500.getMsg());
    }


}
