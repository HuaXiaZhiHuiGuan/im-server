package cn.slb.im.chat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author nick
 */
@EnableDiscoveryClient
@SpringBootApplication
public class ImChatServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ImChatServerApplication.class, args);
	}

}
