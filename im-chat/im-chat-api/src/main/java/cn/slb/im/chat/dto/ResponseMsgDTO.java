package cn.slb.im.chat.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author: NickLi
 * @Description: 消息响应
 * @Date: 2021/12/8 16:52
 */
@Data
public class ResponseMsgDTO implements Serializable {

}
