package cn.slb.im.chat.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author: NickLi
 * @Description: 消息请求
 * @Date: 2021/12/8 16:51
 */
@Data
public class RequestMsgDTO implements Serializable {

}
