package cn.slb.im;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author nick
 */
@SpringBootApplication
public class ImClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(ImClientApplication.class, args);
	}

}
