package cn.slb.im.controller;

import cn.slb.im.common.utils.R;
import cn.slb.im.feign.UserFeignService;
import cn.slb.im.user.dto.LoginUserDTO;
import com.alibaba.fastjson.JSON;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

/**
 * @author nick
 */
@Slf4j
@RestController
@AllArgsConstructor
public class AuthController {

    private final UserFeignService userFeignService;


    @ResponseBody
    @PostMapping("/login")
    public R login(@RequestBody LoginUserDTO loginUser, HttpSession session) {
        log.info("【用户登录】请求参数：{}", JSON.toJSONString(loginUser));
        R r = userFeignService.login(loginUser);
        log.info("【用户登录】返回参数：{}", JSON.toJSONString(r));
        session.setAttribute("user", r.getData());
        return r;
    }

}
