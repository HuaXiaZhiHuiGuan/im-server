package cn.slb.im.feign;


import cn.slb.im.user.api.UserApi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 用户服务 feign调用
 *
 * @author nick
 */

@FeignClient(value = "im-user", path = "/user", contextId = "UserFeignService")
public interface UserFeignService extends UserApi {

}
